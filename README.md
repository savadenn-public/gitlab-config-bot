# GitLab config bot

This bot is designed to add Infrastructure as Code to GitLab usage.

## Run it

1. Creates a `.env` file at project root :
   ```env
   PERSONAL_GITLAB_TOKEN=<api-token>
   ```
2. Run
   ```bash
   make run
   ```

## GitLab Graph Explorer

[GitLab GraphQL Explorer](https://gitlab.com/-/graphql-explorer)
