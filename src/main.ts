import './moment'
import moment from 'moment-timezone'
import * as GitLab from './gitlab'
import chalk from 'chalk'
import { alert, log } from './logger'
import { createDir, MUTATIONS_PATHS } from './Paths'
import { configuration } from '@/CheckEnv'

/**
 * Process a given group
 */
async function processGroup(params: GitLab.GitLabGroupRelated) {
  const group = await GitLab.Group.getGroup(params)
  if (!group) {
    console.log(chalk.red(`Group not found for path ${params.groupPath}`))
    return
  }

  log({ prefix: group.name, message: 'Beginning group' })

  await GitLab.Iteration.configureNext({
    ...params,
    count: 30,
    from: moment().toDate(),
  })

  await GitLab.Issue.setMissingIteration(params)

  // const string = await Labels.dumpAll({ groupPath: group })
  //
  // await sprintHandler.configureNext({ group, number: 2 })
}

/**
 * Process each folder in data
 */
async function main() {
  if (configuration.modeApply) {
    alert({ message: 'Mutations are directly sent', prefix: 'APPLY MODE' })
  } else {
    alert({ message: 'Mutation are NOT sent', prefix: 'DRY MODE' })
  }

  log({ message: `Mutations are logged to ${MUTATIONS_PATHS}` })
  await createDir({ path: MUTATIONS_PATHS, clean: true })

  log({
    prefix: 'MAIN',
    message: `Launching for ${configuration.groupPath}`,
  })
  await processGroup({ groupPath: configuration.groupPath })
}

main()
