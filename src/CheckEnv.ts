import { alert } from '@/logger'

/**
 * Check all required env variables
 */
export function checkEnv(): void {
  if (!process.env.PERSONAL_GITLAB_TOKEN && !process.env.CI_JOB_TOKEN) {
    alert({
      prefix: 'env',
      message:
        'Missing environment variable PERSONAL_GITLAB_TOKEN or CI_JOB_TOKEN',
    })
    process.exit(1)
  }

  if (!process.argv[2]) {
    alert({
      prefix: 'cmd',
      message: 'Missing target group_path (arg 2)',
    })
    process.exit(1)
  }
}

checkEnv()
export const configuration = {
  token: process.env.PERSONAL_GITLAB_TOKEN || process.env.CI_JOB_TOKEN,
  runAsJob: !process.env.PERSONAL_GITLAB_TOKEN && process.env.CI_JOB_TOKEN,
  modeApply: process.env.MODE === 'APPLY',
  groupPath: process.argv[2],
}
