/**
 * Is relative to a group
 */
export interface GitLabGroupRelated {
  /**
   * Group URI path
   *
   * Ex : `savadenn-public` in https://gitlab.com/savadenn-public
   */
  groupPath: string
}
