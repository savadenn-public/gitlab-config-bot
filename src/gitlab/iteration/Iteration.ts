/**
 * A theoretical iteration
 */
export interface IterationTemplate {
  title: string
  startDate: string
  dueDate: string

  /**
   * Markdown compatible description
   */
  description: string
}

/**
 * An iteration from GitLab
 */
export interface Iteration extends IterationTemplate {
  id: string
}
