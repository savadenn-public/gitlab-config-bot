import * as GraphQL from '@/gitlab/graphQL'
import { log } from '@/logger'
import { IterationByStart } from '.'
import { gql } from 'graphql-request'
import { GitLabGroupRelated } from '@/gitlab'

const GET_ALL_ITERATION = gql`
  query getIterations($groupPath: ID!, $start: Date!, $end: Date!) {
    group(fullPath: $groupPath) {
      iterations() {
        nodes {
          id
          startDate
          dueDate
        }
      }
    }
  }
`

/**
 * Get all iterations
 * @param params
 */
export async function getAll(
  params: GitLabGroupRelated
): Promise<IterationByStart> {
  const client = await GraphQL.getClient()
  const data = await client.request(GET_ALL_ITERATION, params)
  const iterations: IterationByStart = {}

  for (const iteration of data?.group?.iterations?.nodes || []) {
    iterations[iteration.dueDate] = iteration
  }

  log({
    prefix: params.groupPath,
    message: `Found ${data?.group?.iterations?.nodes?.length} iterations`,
  })

  return iterations
}
