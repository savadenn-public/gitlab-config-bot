import { GitLabGroupRelated } from '@/gitlab'
import moment from 'moment-timezone'
import { IterationTemplate } from '.'
import { TIMEZONE } from '@/moment'

const FULL_PATH_PATTERN = '##FULL_PATH##'
const DESCRIPTION = `## Sprint start

1. Check %"Backlog" for ticket to add
1. Check milestone

## Sprint review

1. Review closed issues
1. Review on-going issue
1. Review tensions
1. List closed and on-going MR
1. [Tickets flag for discussion](https://gitlab.com/groups/${FULL_PATH_PATTERN}/-/issues?label_name%5B%5D=sprint_review)
`

/**
 * Params to get an iteration template
 */
export interface TemplateParams extends GitLabGroupRelated {
  /**
   * Any day of the iteration
   */
  date: Date
}

/**
 * Generate iteration templates
 */
export function generateTemplate(params: TemplateParams): IterationTemplate {
  const dueDate = moment.tz(params.date, TIMEZONE).endOf('week')
  const formattedDueDate = dueDate.format('YYYY-MM-DD')
  return {
    title: formattedDueDate,
    startDate: dueDate.clone().startOf('week').format('YYYY-MM-DD'),
    dueDate: formattedDueDate,
    description: DESCRIPTION.replace(FULL_PATH_PATTERN, params.groupPath),
  }
}

export interface FuturTemplatesParams extends GitLabGroupRelated {
  /**
   * Number of futur iterations to generate
   */
  count: number

  /**
   * Date to generate from
   */
  from: Date
}

type TemplatesByStart = Record<
  IterationTemplate['startDate'],
  IterationTemplate
>

/**
 * Generates n next iterations
 */
export async function generateFuturTemplates(
  params: FuturTemplatesParams
): Promise<TemplatesByStart> {
  const dueDate = moment(params.from).endOf('week')
  const iterations: TemplatesByStart = {}
  for (let i = 0; i < params.count; i++) {
    const iteration = generateTemplate({
      groupPath: params.groupPath,
      date: dueDate.toDate(),
    })
    iterations[iteration.dueDate] = iteration
    dueDate.add(1, 'week')
  }
  return iterations
}
