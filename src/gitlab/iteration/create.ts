import { IterationTemplate } from '.'
import { log } from '@/logger'
import { gql } from 'graphql-request'
import * as GraphQL from '@/gitlab/graphQL'
import { GitLabGroupRelated } from '@/gitlab'

const CREATE_ITERATION = gql`
  mutation CreateIter($input: CreateIterationInput!) {
    createIteration(input: $input) {
      iteration {
        id
      }
      errors
    }
  }
`

/**
 * Dataset to create a sprint in GitLab
 */
export interface IterationCreation
  extends IterationTemplate,
    GitLabGroupRelated {}

/**
 * Creates on iteration on GitLab
 * @param params
 * @private
 */
export async function create(
  params: IterationCreation
): Promise<string | undefined> {
  log({
    prefix: params.groupPath,
    message: `Creating iteration ${params.title}`,
  })

  const response = await GraphQL.mutate<{
    createIteration: {
      iteration: { id: string }
      errors?: string[]
    }
  }>({
    resource: GraphQL.Resources.Iteration,
    groupPath: params.groupPath,
    identifier: params.startDate,
    type: GraphQL.MutationTypes.Create,
    query: CREATE_ITERATION,
    variables: { input: params },
  })
  if (response) {
    const errors = response?.createIteration?.errors
    if (errors?.length) {
      throw new Error(errors.join('\n'))
    }
    return response.createIteration.iteration.id
  }
}
