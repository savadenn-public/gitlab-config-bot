import moment from 'moment-timezone'
import { log } from '@/logger'
import {
  create,
  FuturTemplatesParams,
  generateFuturTemplates,
  getByPeriod,
  patch,
} from '.'

/**
 * Configure n next sprints if not exists
 */
export async function configureNext(
  params: FuturTemplatesParams
): Promise<void> {
  const iterations = await getByPeriod({
    groupPath: params.groupPath,
    start: moment().startOf('week').format('YYYY-MM-DD'),
    end: moment().add(params.count, 'week').endOf('week').format('YYYY-MM-DD'),
  })

  log({
    prefix: params.groupPath,
    message: `Found ${iterations.length} iterations`,
  })

  const total = { update: 0, create: 0, ok: 0 }

  for (const template of Object.values(await generateFuturTemplates(params))) {
    const existingIteration = iterations[template.dueDate]

    if (existingIteration) {
      const shouldUpdate = await patch({
        groupPath: params.groupPath,
        iteration: existingIteration,
        template: template,
      })
      if (shouldUpdate) {
        total.update++
      } else {
        total.ok++
      }
    } else {
      await create({
        groupPath: params.groupPath,
        ...template,
      })
      total.create++
    }
  }
  log({ prefix: params.groupPath, message: 'Iterations Summary', data: total })
}
