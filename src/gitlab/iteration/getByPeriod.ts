import * as GraphQL from '@/gitlab/graphQL'
import { Iteration } from '.'
import { gql } from 'graphql-request'
import { GitLabGroupRelated } from '@/gitlab'

export type IterationByStart = Record<Iteration['startDate'], Iteration>

const GET_NEXT_ITERATIONS = gql`
  query getIterations($groupPath: ID!, $start: Date!, $end: Date!) {
    group(fullPath: $groupPath) {
      iterations(timeframe: { start: $start, end: $end }) {
        nodes {
          id
          title
          startDate
          dueDate
          description
        }
      }
    }
  }
`

interface Period extends GitLabGroupRelated {
  start: string
  end: string
}

/**
 * Query GitLab to retrieve current and upcoming iterations
 * @param params
 */
export async function getByPeriod(params: Period): Promise<IterationByStart> {
  const client = await GraphQL.getClient()
  const data = await client.request(GET_NEXT_ITERATIONS, params)
  const iterations: IterationByStart = {}

  for (const iteration of data?.group?.iterations?.nodes || []) {
    iterations[iteration.dueDate] = iteration
  }

  return iterations
}
