import { GitLabGroupRelated } from '@/gitlab'
import { Iteration } from '@/gitlab/iteration/Iteration'
import * as GraphQL from '@/gitlab/graphQL'
import moment from 'moment-timezone'
import { TIMEZONE } from '@/moment'

import { gql } from 'graphql-request'

const GET_ITERATION = gql`
  query getIterations($groupPath: ID!, $date: Date!) {
    group(fullPath: $groupPath) {
      iterations(timeframe: { start: $date, end: $date }) {
        nodes {
          id
          title
          startDate
          dueDate
          description
        }
      }
    }
  }
`

interface ExactDate extends GitLabGroupRelated {
  date: string
}

const iterationCache: Map<string, Map<string, Iteration>> = new Map<
  string,
  Map<string, Iteration>
>()

/**
 * Get cache instance for current group
 * @param params
 */
function getGroupCache(params: GitLabGroupRelated): Map<string, Iteration> {
  let groupCache = iterationCache.get(params.groupPath)
  if (!groupCache) {
    groupCache = new Map<string, Iteration>()
    iterationCache.set(params.groupPath, groupCache)
  }
  return groupCache
}

/**
 * Query GitLab to retrieve iteration for a specific date
 * @param params
 */
async function queryByDate(params: ExactDate): Promise<Iteration | undefined> {
  const client = await GraphQL.getClient()
  const data = await client.request(GET_ITERATION, params)

  const nodes = data?.group?.iterations?.nodes
  if (nodes && nodes.length > 0) {
    return nodes[0]
  }
  return undefined
}

/**
 * Query GitLab to retrieve iteration for a specific date
 * @param params
 */
export async function getByDate(
  params: ExactDate
): Promise<Iteration | undefined> {
  const cache = getGroupCache(params)
  const date = moment(params.date).tz(TIMEZONE).format('YYYY-MM-DD')
  if (cache.has(date)) {
    return cache.get(date)
  }

  const iteration = await queryByDate(params)
  if (iteration) {
    const date = moment.tz(iteration.startDate, TIMEZONE)
    const end = moment.tz(iteration.dueDate, TIMEZONE)
    do {
      cache.set(date.format('YYYY-MM-DD'), iteration)
      date.add(1, 'day')
    } while (!date.isAfter(end, 'day'))
  }

  return iteration
}
