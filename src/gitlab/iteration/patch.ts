import { log } from '@/logger'
import { Iteration, IterationTemplate } from '.'
import { GitLabGroupRelated } from '@/gitlab'
import * as GraphQL from '@/gitlab/graphQL'
import { gql } from 'graphql-request'

const UPDATE_ITERATION = gql`
  mutation UpdateIter($input: UpdateIterationInput!) {
    updateIteration(input: $input) {
      errors
    }
  }
`

/**
 * Dataset for iteration Update
 */
export interface IterationUpdate
  extends Partial<IterationTemplate & GitLabGroupRelated> {
  /**
   * Required
   */
  id: string

  /**
   * Required
   */
  groupPath: string
}

/**
 * Dataset to required an update on
 */
export interface UpdateIterationParams extends GitLabGroupRelated {
  /**
   * Iteration current values
   */
  iteration: Iteration

  /**
   * Values to apply to iteration
   */
  template: IterationTemplate
}

/**
 * Updates an iteration on GitLab on needed fields
 *
 * @param params
 * @private
 */
export async function patch(params: UpdateIterationParams): Promise<boolean> {
  const input: IterationUpdate = {
    id: params.iteration.id,
    groupPath: params.groupPath,
  }

  let shouldUpdate = false
  const properties: Array<keyof IterationTemplate> = [
    'title',
    'startDate',
    'dueDate',
    'description',
  ]

  for (const property of properties) {
    if (params.iteration[property] !== params.template[property]) {
      shouldUpdate = true
      input[property] = params.template[property]
    }
  }

  if (shouldUpdate) {
    log({
      prefix: params.groupPath,
      message: `Updating iteration ${params.template.title}`,
      data: { id: params.iteration.id },
    })

    const response = await GraphQL.mutate<{
      updateIteration: {
        errors?: string[]
      }
    }>({
      resource: GraphQL.Resources.Iteration,
      groupPath: params.groupPath,
      identifier: input.id,
      type: GraphQL.MutationTypes.Update,
      query: UPDATE_ITERATION,
      variables: { input },
    })

    if (response) {
      const errors = response['updateIteration'].errors
      if (errors?.length) {
        throw new Error(errors.join('\n'))
      }
    }
  }
  return shouldUpdate
}
