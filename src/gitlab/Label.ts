import { gql } from 'graphql-request'
import { dump } from 'js-yaml'
import * as GraphQL from '@/gitlab/graphQL'

const GET_LABELS = gql`
  query getGroupLabels($groupPath: ID!, $after: String) {
    group(fullPath: $groupPath) {
      labels(after: $after) {
        nodes {
          description
          title
          color
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`

export interface GitlabLabel {
  description: string
  title: string
  color: string
}

export interface LabelSearch {
  /**
   * Gitlab path of group (as in URL)
   */
  groupPath: string

  /**
   * Search only after this cursor (use with endCursor pagination)
   */
  after?: string
}

/**
 * Get all labels of a group
 */
async function getLabels(params: LabelSearch): Promise<GitlabLabel[]> {
  const client = await GraphQL.getClient()
  const data = await client.request(GET_LABELS, params)
  const labels = data?.group?.labels?.nodes
  if (labels) {
    if (data?.group?.labels?.pageInfo?.hasNextPage) {
      labels.push(
        ...(await this.getLabels({
          ...params,
          after: data.group.labels.pageInfo.endCursor,
        }))
      )
    }
    return labels
  } else {
    return []
  }
}

/**
 * Dump all labels of a group in file
 */
async function dumpAll(params: LabelSearch) {
  const labels = await this.getLabels(params)
  const sortLabels: Record<GitlabLabel['title'], Partial<GitlabLabel>> = {}
  labels.forEach((label: GitlabLabel) => {
    const { color, description } = label
    sortLabels[label.title] = { color, description }
  })

  return dump(sortLabels)
}
