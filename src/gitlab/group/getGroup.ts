import { gql } from 'graphql-request'
import { GitLabGroupRelated, GraphQL } from '@/gitlab'
import { Group } from '.'

const GET_GROUP = gql`
  query getGroup($fullPath: ID!) {
    group(fullPath: $fullPath) {
      id
      name
    }
  }
`

/**
 * Get group from GitLab
 * @param params
 */
export async function getGroup(
  params: GitLabGroupRelated
): Promise<Group | undefined> {
  const client = await GraphQL.getClient()
  const dataQL = await client.request(GET_GROUP, { fullPath: params.groupPath })
  if (dataQL && dataQL.group) {
    return {
      ...dataQL.group,
      apiId: dataQL.group.id.replace(''),
    }
  }
}
