/**
 * GitLab Group
 */
export interface Group {
  /**
   * GraphQL id
   */
  id: string

  /**
   * API id
   */
  apiId: string

  /**
   * Name of the group
   */
  name: string
}
