import { Gitlab } from '@gitbeaker/node'
import chalk from 'chalk'
import { configuration } from '@/CheckEnv'

const GITLAB_ENDPOINT = 'https://gitlab.com/'

/**
 * Creates new connector to GitLab API
 * API Token passed as environment variable
 */
function newClient() {
  return new Gitlab({
    host: GITLAB_ENDPOINT,
    token: configuration.token,
  })
}

/**
 * API client instance
 */
let client: ReturnType<typeof newClient> | undefined = undefined

/**
 * Retrieve instance of client
 */
export async function getClient(): Promise<ReturnType<typeof newClient>> {
  if (!client) {
    client = newClient()
    const data = await client.Users.current()
    if (data) {
      console.log(
        chalk.blue(
          `Logged via API as ${(data as Record<string, unknown>).name}`
        )
      )
    } else {
      throw new Error('Invalid credential, no user match')
    }
  }
  return client
}
