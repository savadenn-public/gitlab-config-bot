export const MutationTypes = {
  Create: 'create',
  Update: 'update',
}

export type MutationType =
  | typeof MutationTypes.Create
  | typeof MutationTypes.Update
