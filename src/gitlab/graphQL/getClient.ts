import { GraphQLClient } from 'graphql-request'
import chalk from 'chalk'
import { configuration } from '@/CheckEnv'

const GITLAB_GRAPHQL_ENDPOINT = 'https://gitlab.com/api/graphql'

/**
 * Creates a client for graphQL
 * API Token passed as environment variable
 */
function newClient(): GraphQLClient {
  return new GraphQLClient(GITLAB_GRAPHQL_ENDPOINT, {
    headers: {
      Authorization: `Bearer ${configuration.token}`,
      ContentType: 'application/json',
    },
  })
}

/**
 * GraphQL client instance
 */
let client: ReturnType<typeof newClient> | undefined = undefined

/**
 * Retrieve instance of client
 */
export async function getClient(): Promise<GraphQLClient> {
  if (!client) {
    client = newClient()
    const data = await client.request('query {currentUser {name}}')
    if (data.currentUser) {
      console.log(chalk.blue(`Logged via GraphQL as ${data.currentUser.name}`))
    } else {
      throw new Error('Invalid credential, no user match')
    }
  }
  return client
}
