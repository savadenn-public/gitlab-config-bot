import { GitLabGroupRelated } from '@/gitlab'
import { Variables } from 'graphql-request/dist/types'
import { MutationType, Resource } from '.'

/**
 * A mutation order
 */
export interface MutationOrder extends GitLabGroupRelated {
  /**
   * Resource
   */
  resource: Resource

  /**
   * Type of mutation
   */
  type: MutationType

  /**
   * GraphQL Query to run
   */
  query: string

  /**
   * Identifier of the resource (to log the file)
   */
  identifier: string

  /**
   * Variables to inject
   */
  variables: Variables
}
