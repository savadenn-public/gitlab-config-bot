import createRequestBody from 'graphql-request/dist/createRequestBody'
import { getPath, writeFile } from '@/Paths'
import { getClient, MutationOrder } from '.'
import { configuration } from '@/CheckEnv'

/**
 * Registers a mutation to apply to GitLab
 * @param params
 */
export async function mutate<Response>(
  params: MutationOrder
): Promise<Response | undefined> {
  const mutation = createRequestBody(params.query, params.variables)
  const filename = params.identifier + '.json'
  const path = getPath({
    groupPath: params.groupPath,
    resource: params.resource,
    type: params.type,
    filename,
  })
  await writeFile({ path, data: mutation.toString() })

  if (configuration.modeApply) {
    const client = await getClient()
    return await client.request<Response>(params.query, params.variables)
  }
}
