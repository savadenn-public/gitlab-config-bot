export const Resources = {
  Iteration: 'iteration',
  Issue: 'Issue',
}

export type Resource = typeof Resources.Iteration
