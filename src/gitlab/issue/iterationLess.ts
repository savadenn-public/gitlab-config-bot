import { gql } from 'graphql-request'
import { GitLabGroupRelated } from '@/gitlab'
import * as GraphQL from '@/gitlab/graphQL'

export const FULL_PATH_SPLIT = '/-/issues/'
const ISSUES_WITHOUT_ITERATION = gql`
  query listIterationLessIssues($groupPath: ID!, $after: String) {
    group(fullPath: $groupPath) {
      issues(
        state: closed
        iterationWildcardId: NONE
        after: $after
        includeSubgroups: true
      ) {
        nodes {
          closedAt
          webPath
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`

/**
 * Variable for GraphQL query
 */
export interface IterationLessQuery extends GitLabGroupRelated {
  /**
   * Pagination for issue
   */
  after?: string
}

/**
 * Dataset for issue without iteration
 */
export interface IterationLessIssue {
  projectPath: string
  iid: string
  closedAt: string
}

export interface IterationLessResult extends IterationLessQuery {
  issues: IterationLessIssue[]
}

/**
 * List issues without iteration (return object may be used to query next issues)
 * @param params
 */
export async function listIterationLess(
  params: IterationLessQuery
): Promise<IterationLessResult> {
  const client = await GraphQL.getClient()
  const data = await client.request(ISSUES_WITHOUT_ITERATION, params)

  const result: IterationLessResult = {
    groupPath: params.groupPath,
    issues: [],
  }

  for (const issue of data?.group?.issues?.nodes || []) {
    const [projectPath, iid] = issue.webPath.split(FULL_PATH_SPLIT)

    result.issues.push({
      projectPath: projectPath.startsWith('/')
        ? projectPath.substring(1)
        : projectPath,
      iid,
      closedAt: issue.closedAt,
    })
  }

  const hasNextPage = data?.group?.issues?.pageInfo?.hasNextPage
  const after = data?.group?.issues?.pageInfo?.endCursor
  if (hasNextPage && after) result.after = after

  return result
}

const COUNT_ISSUES_WITHOUT_ITERATION = gql`
  query countIterationLessIssues($groupPath: ID!) {
    group(fullPath: $groupPath) {
      issues(state: closed, iterationWildcardId: NONE, includeSubgroups: true) {
        count
      }
    }
  }
`

/**
 * Count all issues without iteration
 * @param params
 */
export async function countIterationLess(
  params: GitLabGroupRelated
): Promise<number> {
  const client = await GraphQL.getClient()
  const data = await client.request(COUNT_ISSUES_WITHOUT_ITERATION, params)
  if (data?.group?.issues) {
    return data?.group?.issues?.count
  } else {
    throw new Error(`Failed to retrieve count of issue, ${JSON.parse(data)}`)
  }
}
