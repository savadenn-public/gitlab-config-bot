import * as Api from '@/gitlab/api'

/**
 * List all issues without milestones
 * @param params
 */
export async function listMilestoneLess(params: {
  groupId: string
}): Promise<Record<string, unknown> | Record<string, unknown>[]> {
  const client = await Api.getClient()
  return await client.Issues.all({
    groupId: params.groupId,
    milestone_title: 'None',
  })
}
