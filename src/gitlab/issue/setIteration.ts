import { GitLabGroupRelated } from '@/gitlab'
import {
  countIterationLess,
  FULL_PATH_SPLIT,
  IterationLessResult,
  listIterationLess,
} from '.'
import { alert, log } from '@/logger'
import { Iteration } from '..'
import { gql } from 'graphql-request'
import * as GraphQL from '@/gitlab/graphQL'
import moment from 'moment-timezone'
import { configuration } from '@/CheckEnv'

const SET_ITERATION_ON_ISSUE = gql`
  mutation setIteration($input: IssueSetIterationInput!) {
    issueSetIteration(input: $input) {
      errors
    }
  }
`

interface IssueSetIterationInput {
  /**
   * Full URI path of the project the issue to mutate is in
   */
  projectPath: string

  /**
   * The IID of the issue to mutate
   */
  iid: string

  /**
   * The iteration to assign to the issue.
   */
  iterationId: string
}

export type setIterationOrder = IssueSetIterationInput & GitLabGroupRelated

/**
 * Configure iteration on a issue
 * @param params
 */
export async function setIteration(params: setIterationOrder): Promise<void> {
  log({
    prefix: params.groupPath,
    message: `setIteration on issue #${params.iid} of ${params.projectPath}`,
  })

  const input: IssueSetIterationInput = {
    iterationId: params.iterationId,
    iid: params.iid,
    projectPath: params.projectPath,
  }

  const response = await GraphQL.mutate<{
    issueSetIteration: {
      errors?: string[]
    }
  }>({
    resource: GraphQL.Resources.Issue,
    groupPath: params.groupPath,
    identifier: params.projectPath + FULL_PATH_SPLIT + params.iid,
    type: GraphQL.MutationTypes.Update,
    query: SET_ITERATION_ON_ISSUE,
    variables: { input },
  })

  if (response) {
    const errors = response['issueSetIteration'].errors
    if (errors?.length) {
      throw new Error(errors.join('\n'))
    }
  }
}

interface hasTotal {
  total: {
    updated: number
    skipped: number
    failed: number
  }
}

/**
 * Process a page of iteration less issues
 * @param params
 */
async function processPage(params: IterationLessResult & hasTotal) {
  for (const issue of params.issues) {
    const iteration = await Iteration.getByDate({
      groupPath: params.groupPath,
      date: issue.closedAt,
    })
    let iterationId
    if (iteration) {
      iterationId = iteration.id
    } else {
      const iterationTemplate = Iteration.generateTemplate({
        groupPath: params.groupPath,
        date: moment(issue.closedAt).toDate(),
      })
      iterationId = await Iteration.create({
        groupPath: params.groupPath,
        ...iterationTemplate,
      })
    }

    if (iterationId) {
      await setIteration({
        groupPath: params.groupPath,
        iterationId: iterationId,
        iid: issue.iid,
        projectPath: issue.projectPath,
      })
      params.total.updated++
    } else if (configuration.modeApply) {
      params.total.failed++
      alert({
        prefix: params.groupPath,
        message: `Fail retrieving iterationId for ${issue.closedAt}`,
      })
    } else {
      params.total.skipped++
      log({
        prefix: params.groupPath,
        message: `Skipping, Cannot create missing iteration in dry-mode`,
      })
    }
  }
}

/**
 * Sets iteration of closed date on issue without iteration
 * @param params
 */
export async function setMissingIteration(
  params: GitLabGroupRelated
): Promise<void> {
  const count = await countIterationLess(params)
  log({
    prefix: params.groupPath,
    message: `Found ${count} closed issues without iteration`,
  })
  if (!count) return

  let iterationLessResult = undefined
  const total = {
    updated: 0,
    skipped: 0,
    failed: 0,
  }
  do {
    iterationLessResult = await listIterationLess(params)
    await processPage({ ...iterationLessResult, total })
  } while (iterationLessResult.after)

  log({
    prefix: params.groupPath,
    message: 'SetMissingIteration Summary',
    data: total,
  })
}
