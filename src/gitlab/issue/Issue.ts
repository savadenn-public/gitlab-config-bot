export interface Issue {
  closedAt: string
  id: string
  title: string
}
