import Path from 'path'
import { PathLike, promises as fs } from 'fs'
import { GraphQL } from './gitlab/'
import { Resource } from '@/gitlab/graphQL'

export const MUTATIONS_PATHS = Path.join(__dirname, '..', 'mutations')

export interface PathFor {
  groupPath?: string
  resource?: Resource
  type?: GraphQL.MutationType
  filename?: string
}

/**
 * Returns file path
 */
export function getPath(pathFor: PathFor): string {
  const steps = [MUTATIONS_PATHS]
  if (pathFor.groupPath) steps.push(pathFor.groupPath)
  if (pathFor.resource) steps.push(pathFor.resource)
  if (pathFor.type) steps.push(pathFor.type)
  if (pathFor.filename) steps.push(pathFor.filename)

  return Path.join(...steps)
}

/**
 * Creates directory
 * @clean if true, remove existing dir
 */
export async function createDir({
  path,
  clean,
}: {
  path: PathLike
  clean?: true
}): Promise<void> {
  try {
    if (clean) {
      await fs.rm(path, { recursive: true, force: true })
    }
    await fs.mkdir(path, { recursive: true })
  } catch (err) {
    if (err.code !== 'EEXIST') {
      throw err
    }
  }
}

/**
 * Write file (creates needed directory
 */
export async function writeFile({
  path,
  data,
}: {
  path: string
  data: string | Uint8Array
}): Promise<void> {
  await createDir({ path: Path.dirname(path) })
  return fs.writeFile(path, data)
}
