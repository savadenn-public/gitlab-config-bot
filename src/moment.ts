import moment from 'moment-timezone'

export const TIMEZONE = process.env.TIMEZONE || 'Europe/Paris'

moment.locale('fr')
moment.tz.setDefault(TIMEZONE)
