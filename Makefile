#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=${PROJECT_NAME}.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY:
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

clean: ## Cleans up environnement
	@docker-compose down --remove-orphans
	@docker-compose pull || true

install: clean ## Starts dev stack with TDD
	@docker-compose run --rm bot yarn

run.dry: ## Dry-runs the bot (mutation are only logged)
	@docker-compose run --rm bot

run.apply: ## Runs the bot (mutation are logged and sent)
	@docker-compose run --rm -e MODE=APPLY bot

sh: ## Open bash in container loaded with vue-cli
	@docker-compose run --rm bot bash

lint:
	@docker-compose run --rm bot bash -c "yarn run lint"
